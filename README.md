# wpsmultiscan
Short useful script to perform a WPS scan a bulk of wordpress sites from a formatted list in a text file.

TODO:
+ Add the ability to tag on flags for scanning options
+ Better output/progress information
+ FIFO command processing (waiting til one scan finished before starting another)
+ Documentation after all this is updated
