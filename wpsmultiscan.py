#!/usr/bin/env python3
import sys
from subprocess import call

def main():
	
	file = open(sys.argv[1]);

	for line in file:
		url = file.readline().strip();
		print("\nScanning: " + url + "\n");
		call(["wpscan", url]);
		
main();
